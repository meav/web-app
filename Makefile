build:
	npm install -g staticrypt
	staticrypt index.html -d . --config false --remember false --short
	curl -L https://workdrive.zohoexternal.com/external/a14c972f76112fc4896a45596191596cf589105dcb93de1fe823a233e8f077f1/download?directDownload=true | tar xv
	curl -L https://workdrive.zohoexternal.com/external/23f1c7468f90c0bdfda916e20ae2a383dd6e7cccd1223d74cdc9bd76e2644685/download?directDownload=true | tar xv
	curl -L https://github.com/mozilla/pdf.js/releases/download/v4.2.67/pdfjs-4.2.67-dist.zip --output 1.zip
	unzip 1.zip -d pdf
	rm 1.zip
	rm -rf foliate-js
	git clone --depth=1 https://github.com/johnfactotum/foliate-js
	curl -L https://github.com/satorumurmur/bibi/releases/download/v1.2.0/Bibi-v1.2.0.zip --output 1.zip
	unzip 1.zip -d epub
	rm 1.zip
	curl -L https://workdrive.zohoexternal.com/external/66d931fe41ac8a4662f80b1b8713a92661afa3c3d594d38d28668d021a442666/download?directDownload=true | tar --extract --verbose --directory epub/bibi-bookshelf --strip-components=1
